const deleteNews = document.getElementById('deleteNewsButton');

// id="deleteNewsButton" href="" data-id={{news.id}} 

if (deleteNews) {

        deleteNews.addEventListener('click', (e) => {
            e.preventDefault();

            if (confirm('Etes vous sur de vouloir supprimer la news ?')) {

                const id = e.target.getAttribute('data-id');

                fetch(this.getAttribute("href"), {
                    method: 'DELETE'
                }
                ).then(res => {
                    if (res.ok) { window.location = res.url }
                    else { alert(res.error) }
                }
                ).catch(e => alert(e))
            }
        })        

}


// const deleteNews = document.querySelectorAll('#deleteNewsButton');

// console.log(deleteNews);

// if (deleteNews) {
//     for (let i = 0; i < deleteNews.length; i++) {

//         deleteNews[i].addEventListener('click', (e) => {
//             e.preventDefault();
//             if (confirm('Etes vous sur de vouloir supprimer la news ?')) {
//                 const id = e.target.getAttribute('data-id');

//                 fetch(`/styles/delete/${id}`, {
//                     method: 'DELETE'
//                 }).then(res => window.location.reload());
//             }
//         })        
//     }
// }