<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use App\Repository\UserRepository;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @UniqueEntity(fields={"email"}, message="There is already an account with this email")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     * @Assert\Email(
     *     message = "Rentrez un email valide."
     * )
     * @Assert\NotBlank
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     * @Assert\Length(min="8", minMessage="Votre mot de passe doit être au minimum long de {{ limit }} caractères")
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=50)
     * @Assert\Length(min="3", minMessage="Votre prénom doit avoir au minimum {{ limit }} caractères")
     * @Assert\NotBlank
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=50)
     * @Assert\Length(min="3", minMessage="Votre prénom doit avoir au minimum {{ limit }} caractères")
     * @Assert\NotBlank
     */
    private $surname;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\Column(type="boolean")
     */
    private $rgpd;

    /**
     * @ORM\Column(type="string", length=70, nullable=true)
     */
    private $activation_token;

    /**
     * @ORM\Column(type="string", length=70, nullable=true)
     */
    private $reset_token;

    /**
     * @ORM\OneToMany(targetEntity=Product::class, mappedBy="seller", orphanRemoval=true, cascade={"remove"})
     */
    private $products;

    /**
     * @ORM\Column(type="string", length=13, nullable=true)
     * @Assert\Length(min=10, minMessage="non complet, il manque des chiffres", max=13)
     */
    private $phone;

    /**
     * @ORM\OneToMany(targetEntity=Message::class, mappedBy="from_id", orphanRemoval=true, cascade={"remove"})
     * @ORM\OrderBy({"created_at" = "DESC"})
     */
    private $message_from;

    /**
     * @ORM\OneToMany(targetEntity=Message::class, mappedBy="to_id", orphanRemoval=true, cascade={"remove"})
     * @ORM\OrderBy({"created_at" = "DESC"})
     */
    private $message_to;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isBlocked = false;


    public function __construct()
    {
        $this->products = new ArrayCollection();
        $this->messages_from = new ArrayCollection();
        $this->message_to = new ArrayCollection();
        $this->message_from = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        $this->plainPassword = null;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSurname(): ?string
    {
        return $this->surname;
    }

    public function setSurname(string $surname): self
    {
        $this->surname = $surname;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getRgpd(): ?bool
    {
        return $this->rgpd;
    }

    public function setRgpd(bool $rgpd): self
    {
        $this->rgpd = $rgpd;

        return $this;
    }

    public function getActivationToken(): ?string
    {
        return $this->activation_token;
    }

    public function setActivationToken(?string $activation_token): self
    {
        $this->activation_token = $activation_token;

        return $this;
    }

    public function getResetToken(): ?string
    {
        return $this->reset_token;
    }

    public function setResetToken(?string $reset_token): self
    {
        $this->reset_token = $reset_token;

        return $this;
    }

    /**
     * @return Collection|Product[]
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }

    public function addProduct(Product $product): self
    {
        if (!$this->products->contains($product)) {
            $this->products[] = $product;
            $product->setSeller($this);
        }

        return $this;
    }

    public function removeProduct(Product $product): self
    {
        if ($this->products->contains($product)) {
            $this->products->removeElement($product);
            // set the owning side to null (unless already changed)
            if ($product->getSeller() === $this) {
                $product->setSeller(null);
            }
        }

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }


    /**
     * @return Collection|Message[]
     */
    public function getMessageTo(): Collection
    {
        return $this->message_to;
    }

    public function addMessageTo(Message $messageTo): self
    {
        if (!$this->message_to->contains($messageTo)) {
            $this->message_to[] = $messageTo;
            $messageTo->setToId($this);
        }

        return $this;
    }

    public function removeMessageTo(Message $messageTo): self
    {
        if ($this->message_to->contains($messageTo)) {
            $this->message_to->removeElement($messageTo);
            // set the owning side to null (unless already changed)
            if ($messageTo->getToId() === $this) {
                $messageTo->setToId(null);
            }
        }

        return $this;
    }


    /**
     * @return Collection|Message[]
     */
    public function getMessageFrom(): Collection
    {
        return $this->message_from;
    }

    public function addMessageFrom(Message $messageFrom): self
    {
        if (!$this->message_from->contains($messageFrom)) {
            $this->message_from[] = $messageFrom;
            $messageFrom->setFromId($this);
        }

        return $this;
    }

    public function removeMessageFrom(Message $messageFrom): self
    {
        if ($this->message_from->contains($messageFrom)) {
            $this->message_from->removeElement($messageFrom);
            // set the owning side to null (unless already changed)
            if ($messageFrom->getFromId() === $this) {
                $messageFrom->setFromId(null);
            }
        }

        return $this;
    }


    
    public function getIsBlocked(): ?bool
    {
        return $this->isBlocked;
    }

    public function setIsBlocked(bool $isBlocked): self
    {
        $this->isBlocked = $isBlocked;

        return $this;
    }

    
}
