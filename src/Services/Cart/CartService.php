<?php

namespace App\Services\Cart;

use App\Repository\ProductRepository;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Service Panier Session
 */
class CartService
{
    /**
     * On prépare une variable protected qui representera la session
     * @var [type]
     */
    protected $session;

    /**
     * On instancie le constructor avec l'appel de la SessionInterface
     * Car indisponible sur les fonctions, mais constructor == OK!
     *
     * @param SessionInterface $session
     */
    public function __construct(SessionInterface $session, ProductRepository $repo) 
    {
        $this->session = $session;
        $this->repo = $repo;
    }

    /**
     * Ajouter un produit au panier
     *
     * @param integer $id
     * @return void
     */
    public function add(int $id) 
    {
        // Initialiser le panier. si non existant le créer ([])
        $cart = $this->session->get('cart', []);  

        //Si le produit existe déjà, augmenter la quantitée
        if (!empty($cart[$id])) {
            $cart[$id]++;
        }
        else {
            // Enregistrer le produit dans le panier (quantité*1)
            $cart[$id] = 1;
        }

        $this->session->set('cart', $cart);
    }

    /**
     * supprimer un produit du panier
     *
     * @param integer $id
     * @return void
     */
    public function remove(int $id) 
    {
        // Récuperer la session 
        $cart = $this->session->get('cart');

        //Si le produit existe, le supprimer
        if (!empty($cart[$id])) {
            unset($cart[$id]);
        }

        // Renvoyer le nouveau panier à la session
        $this->session->set('cart', $cart);
    }

    /**
     * Récuperer le panier avec information Produit et Quantitée
     *
     * @return array
     */
    public function getFullCart() : array
    {
        // Initialisation des variables
        $cart = $this->session->get('cart', []);
        $cartData = [];        

        // Insertion de chaque élément du panier dans cartData (Produit et Quantitée)
        foreach ($cart as $id => $quantity) {
            $cartData[] = [
                'product' => $this->repo->find($id),
                'quantity' => $quantity
            ];
        }

        // On retourne le panier rempli !
        return $cartData;
    }

    /**
     * Calculer et renvoyer le montant total du panier
     *
     * @return float
     */
    public function getTotal() : float 
    {
        // Initialisation de la variable $total a zéro
        $total = 0;

        // Calcul du Prix Total
        foreach ($this->getFullCart() as $item) {
            $total += ($item['product']->getPrice() * $item['quantity']);
        }

        // On retourne le montant total du panier
        return $total;
    }

}
