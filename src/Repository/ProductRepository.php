<?php

namespace App\Repository;

use App\Entity\Product;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }

    /**
     * @return Product[] Returns an array of Product objects
     */
    public function findBySearchBar(string $value): array
    {
        return $this->createQueryBuilder('p')
            ->where('p.active = 1')
            ->andwhere('p.title LIKE :search')
            ->orWhere('p.state LIKE :search')
            ->orWhere('p.author LIKE :search')
            ->setParameter('search', '%'.$value.'%')
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return Product[] Returns an array of Product objects
     */
    public function getActiveProducts(): array
    {
        return $this->createQueryBuilder('p')
            ->where('p.active = :bool')
            ->setParameter('bool', 1)
            ->orderBy('p.created_at','DESC')
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * Get the "$limit" last products, order by DateTime, and active
     *
     * @param integer $limit
     * @return Product[] Returns an array of Product objects
     */
    public function getLastProducts(int $limit): array
    {
        return $this->createQueryBuilder('p')
            ->where('p.active = :bool')
            ->setParameter('bool', 1)
            ->orderBy('p.created_at','DESC')
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * Get products by search form
     *
     * @param string $state
     * @param string $category
     * @param integer $price
     * @return Product[] Returns an array of Product objects
     */
    public function findBySearch(string $state, string $category, int $price): array
    {
        return $this->createQueryBuilder('p')
            ->where('p.active = 1')
            ->andWhere('p.state = :state')
            ->andWhere('p.category = :category')
            ->andWhere('p.price < :price')
            ->setParameter(':price', $price)
            ->setParameter(':state', $state)
            ->setParameter(':category', $category)
            ->getQuery()
            ->getResult()
        ;
    }
    
    /*
    public function findOneBySomeField($value): ?Product
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
