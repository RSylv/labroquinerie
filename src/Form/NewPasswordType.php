<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;

class NewPasswordType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('plainPassword', RepeatedType::class, [
                'attr' => [
                    'ng-model' => 'password',
                    'ng-change' => 'analyze(password)',
                ],
                'type' => PasswordType::class,
                'invalid_message' => 'Les mots de passe doivent être identiques !',
                'required' => true,
                'mapped' => false,
                // Chargement des attributs Angular (StrongPassword)
                'first_options' => ['label' => 'Mot de passe', 
                    'attr' => [
                    'ng-model' => 'password',
                    'ng-change'=> 'analyze(password)'
                ]],
                'second_options' => ['label' => 'Confirmez le Mot de passe'],
                'constraints' => [
                    // instead of being set onto the object directly,
                    // this is read and encoded in the controller
                    new NotBlank([
                        'message' => 'Entrez un mot de passe',
                    ]),
                    new Length([
                        'min' => 8,
                        'minMessage' => 'Votre password doit au minimum avoir {{ limit }} caractères',
                        // max length allowed by Symfony for security reasons
                        'max' => 255,
                    ]),
                    new Regex([
                        'pattern' => '^[a-zA-Z!"#\$%&\'\(\)\*\+,-\.\/:;<=>\?@[\]\^_`\{\|}~]{8}$^',
                        'match' => false,
                        'message' => 'Votre password doit avoir 8 caractères, une majuscule ou un chiffre, une minuscule, ainsi qu\'un caractère spécial au minimum',
                    ])
                ],
            ])
            ->add('send', SubmitType::class, ['label' => 'Enregistrer'])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
