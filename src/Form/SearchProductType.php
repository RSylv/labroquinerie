<?php

namespace App\Form;

use App\Entity\Product;
use App\Entity\Category;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class SearchProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('state', ChoiceType::class, [
                'choices' => [
                    'Neuf'          => 'NEUF',
                    'Très bon état' => 'TRES BON ETAT',
                    'Bon état'      => 'BON ETAT',
                    'Etat moyen'    => 'ETAT MOYEN',
                    'Abimé'         => 'ABIME',
                ],
                'constraints' => [
                    new NotBlank([
                        'message' => 'Entrez un nom',
                    ]),
                ],
                'label'     => 'Etat',
                'required'  => true,
            ])
            ->add('price', MoneyType::class, [
                'attr' => [
                    'min'   => 0,
                    'max'   => 1000,
                ],
                'label'     => 'Prix Maximum',
                'required'  => false,
            ])
            ->add('category', EntityType::class, [
                'label'        => 'Categorie',
                'class'        => Category::class,
                'choice_label' => 'name',
                'constraints' => [
                    new NotBlank([
                        'message' => 'Entrez un nom',
                    ]),
                ],
                'required'     => true,               
            ])
            ->add('search', SubmitType::class, ['label' => 'Rechercher !'] )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // 'data_class' => Product::class,
        ]);
    }
}
