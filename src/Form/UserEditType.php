<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class UserEditType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Prénom',
                'constraints' => [                    
                    new Regex([
                        'pattern' => '^(?:[\w-]+)$^',
                        'message' => 'Caractères spéciaux non autorisés, hormis le tiret, ex: Jean-Michel26',
                    ])
                ],
            ])
            ->add('surname', TextType::class, [
                'label' => 'Nom',
                'constraints' => [                    
                    new Regex([
                        'pattern' => '^(?:[\w-]+)$^',
                        'message' => 'Caractères spéciaux non autorisés, hormis le tiret, ex: Dumas10',
                    ])
                ],
            ])         
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
