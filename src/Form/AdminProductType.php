<?php

namespace App\Form;

use App\Entity\Product;
use App\Entity\Category;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class AdminProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('category', EntityType::class, [
                'class' => Category::class,
                'choice_label' => 'name'                
            ])
            ->add('title')
            ->add('author')
            ->add('state', ChoiceType::class, [
                'choices' => [
                    'Neuf' => 'NEUF',
                    'Très bon état' => 'TRES BON ETAT',
                    'Bon état' => 'BON ETAT',
                    'Etat moyen' => 'ETAT MOYEN',
                    'Abimé' => 'ABIME',
                ],
                'label' => 'Etat',
                'required' => true,
            ])
            ->add('description', TextareaType::class, [
                'label' => 'Décrivez le produit, dites en plus sur son état si vous le souhaitez, ou autre',
                'required' => true,
            ])
            ->add('price', MoneyType::class)
            ->add('quantity', IntegerType::class)
            ->add('active', ChoiceType::class, [
                'choices' => [
                    'YES' => true,
                    'NO' => false
                ]
            ])
            ->add('image', FileType::class, [
                'label' => false,
                'multiple' => true,
                'mapped' => false,  
                'required' => false,
            ])
        ;                
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Product::class,
        ]);
    }
}
