<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TelType;

class SellerType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('phone', TelType::class, [
                'label' => 'téléphone',
                'constraints' => [
                    new NotBlank([
                        'message' => 'Entrez un numéro de téléphone',
                    ]),
                    new Length([
                        'max' => 13,
                        'min' => 10,
                        'minMessage' => 'Votre téléphone doit avoir au minimum {{ limit }} chiffres',
                        'maxMessage' => 'Votre téléphone doit avoir au maximum {{ limit }} chiffres',
                    ]),
                    new Regex([
                        'pattern' => '^(?:(?:\+|00)33|0)\s*[1-9](?:[\s.-]*\d{2}){4}^',
                        'message' => 'format invalide, veuillez saisir un numéro valide ex:0685264752, +33(...) 0033(...)'
                    ])
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }

}
