<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class AdminUserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class, [
                'constraints' => [
                    new NotBlank([
                        'message' => 'Merci de saisir une adresse email !'
                    ]),                    
                ],
                'required' => true
            ])
            ->add('roles', ChoiceType::class, [
                'choices' => [
                    'Administrateur' => 'ROLE_ADMIN',
                    'Vendeur' => 'ROLE_SELLER',
                    'Utilisateur' => 'ROLE_USER',
                ],
                'expanded' => true,
                'multiple' => true,
                'label' => 'Rôles : '
            ])
            ->add('name', TextType::class, [
                'constraints' => [
                    new NotBlank([
                        'message' => 'Merci de saisir un prenom !'
                    ]),                    
                ],
                'required' => true
            ])
            ->add('surname', TextType::class, [
                'constraints' => [
                    new NotBlank([
                        'message' => 'Merci de saisir un nom !'
                    ]),                    
                ],
                'required' => true
            ])
            ->add('activationToken', ChoiceType::class, [
                'label' => 'Valider le compte',
                'choices' => [
                    'Valider le compte ?' => null,
                ],
                'expanded' => true,
                'required' => false,
            ])
            ->add('isBlocked', ChoiceType::class, [
                'label' => 'Bloquer/Débloquer un utilisateur :',                
                'required' => true,
                'choices' => [
                    'Bloquer l\'utilisateur ?' => 1,
                    'Débloquer l\'utilisateur ?' => 0
                ],
                'expanded' => true,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
