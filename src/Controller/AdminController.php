<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Product;
use App\Entity\Category;
use App\Form\ProductType;
use App\Form\CategoryType;
use App\Form\AdminUserType;
use App\Form\AdminProductType;
use App\Repository\UserRepository;
use App\Repository\MessageRepository;
use App\Repository\ProductRepository;
use App\Repository\CategoryRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AdminController extends AbstractController
{
    /**
     * @IsGranted("ROLE_ADMIN")
     * @Route("/admin", name="admin")
     */
    public function index()
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        return $this->render('admin/index.html.twig', [
            'controller_name' => 'AdminController',
        ]);
    }

    /**
     * Get All Users into pannel
     * 
     * @IsGranted("ROLE_ADMIN")
     * @Route("/admin/utilisateurs", name="admin_users")
     *
     * @param UserRepository $repo
     * @return void
     */
    public function users(UserRepository $repo)
    {
        return $this->render('admin/users.html.twig', [
            'users' => $repo->findAll(),
        ]);
    }

    /**
     * Edit User
     * 
     * @IsGranted("ROLE_ADMIN")
     * @Route("/admin/utilisateurs/modifier/{id}", name="admin_user_edit")
     *
     * @param User $user
     * @return void
     */
    public function editUser(User $user, Request $request) 
    {
        $form = $this->createForm(AdminUserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($user);
            $em->flush();

            $this->addFlash('success', 'l\'Utilisateur "'. $user->getName() .' '.$user->getSurname().'" a été modifié avec succès !');
            return $this->redirectToRoute('admin_users');
        }

        return $this->render('admin/edituser.html.twig', [
            'user' => $user,
            'formUserEdit' => $form->createView()
        ]);
    }

    /**
     * Delete a User in admin panel or in user-account
     * 
     * @IsGranted("ROLE_ADMIN")
     * @Route("/admin/utilisateurs/supprimer/{id<[0-9]+>}", name="admin_remove_user", methods={"DELETE"})
     *
     * @param Request $request
     * @param User $user
     * @return void
     */
    public function deleteUser(Request $request, User $user) 
    {
        // On vérifie Si le token 'delete(id)' est correct :
        if ($this->isCsrfTokenValid('delete'.$user->getId(), $request->request->get('_token'))) {

            
            // On verifie si l'user a un role vendeur (ROLE_SELLER) :
            if ($user->getRoles() === "ROLE_SELLER") {

                // SI l'user a des produits les supprimer
                $products = $user->getProducts();
                if ($products) {
                    foreach ($products as $product) {
                        $product->getMessages();
                        $user->removeProduct($product);
                    }
                }
                
            }
            
            // On récupere l'Entity Manager de Doctrine, puis on lance la requete sql :
            $em = $this->getDoctrine()->getManager();
            $em->remove($user);
            $em->flush();

            // Message de confirmation et redirection :
            $this->addFlash('success', 'L\'utilisateur a bien été supprimé.');
            return $this->redirectToRoute('admin_users');
        }
        else {
            // return new JsonResponse(['error' => 'Le code de suppression est invalide.'], 400);
            $this->addFlash('error', 'Erreur d\'authentification durant la suppression.');
        }

        // Redirection si l'Admin arrive ici, sans passer par le form :
        return $this->redirectToRoute('main');
    }

    /**
     * Get All Products into pannel
     * 
     * @IsGranted("ROLE_ADMIN")
     * @Route("/admin/produits", name="admin_products")
     *
     * @param ProductRepository $repo
     * @return void
     */
    public function products(ProductRepository $repo) 
    {
        return $this->render('admin/products.html.twig', [
            'products' => $repo->findAll(),
        ]);
    }

    /**
     * Edit Product
     * 
     * @IsGranted("ROLE_ADMIN")
     * @Route("/admin/produits/modifier/{id}", name="admin_product_edit")
     *
     * @param Product $product
     * @param Request $request
     * @return void
     */
    public function editProduct(Product $product, Request $request) 
    {
        $form = $this->createForm(AdminProductType::class, $product);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($product);
            $em->flush();

            $this->addFlash('success', 'Le produit "'.$product->getTitle().'" a bien été mis a jour.');
            return $this->redirectToRoute('admin_products');
        }

        return $this->render('admin/editproduct.html.twig', [
            'product' => $product,
            'formProduct' => $form->createView(),
        ]);
    }

    /**
     * Get All Categories into pannel
     * 
     * @IsGranted("ROLE_ADMIN")
     * @Route("/admin/categories", name="admin_categories")
     *
     * @param CategoryRepository $repo
     * @return void
     */
    public function categories(CategoryRepository $repo) 
    {
        return $this->render('admin/categories.html.twig', [
            'categories' => $repo->findAll(),
        ]);
    }

    /**
     * Edit Category
     * 
     * @IsGranted("ROLE_ADMIN")
     * @Route("/admin/categorie/modifier/{id}", name="admin_category_edit")
     *
     * @param Category $category
     * @param Request $request
     * @return void
     */
    public function editCategory(Category $category, Request $request) 
    {
        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($category);
            $em->flush();

            $this->addFlash('success', 'La categorie "'.$category->getName().'" a bien été mise a jour.');
            return $this->redirectToRoute('admin_categories');
        }

        return $this->render('admin/editcategory.html.twig', [
            'category' => $category,
            'formCategory' => $form->createView(),
        ]);
    }
    
    /**
     * Get All Messages into pannel
     * 
     * @IsGranted("ROLE_ADMIN")
     * @Route("/admin/messages", name="admin_messages")
     *
     * @param CategoryRepository $repo
     * @return void
     */
    public function messages(MessageRepository $repo) 
    {
        return $this->render('admin/messages.html.twig', [
            'messages' => $repo->findAll(),
        ]);
    }

    
}
