<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Message;
use App\Form\SellerType;
use App\Form\UserEditType;
use App\Form\SellerEditType;
use App\Repository\MessageRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AccountController extends AbstractController
{
    /**
     * @IsGranted("ROLE_USER")
     * @Route("/mon-compte/{id}", name="my_account")
     *
     * @param integer $id
     * @param EntityManagerInterface $em
     * @return void
     */
    public function index(int $id, Request $request, EntityManagerInterface $em, MessageRepository $repo)
    {
        // On recupere l'user present 
        $user = $this->getUser();

        // Si l'user n'est pas celui attendu, on redirige la page
        if ($id !== $user->getId()) {
            return $this->redirectToRoute('my_account', ['id' => $user->getId()]);
        }
        // Si l'user n'a pas valider son compte
        if ($user->getActivationToken() !== null) {
            $this->addFlash('warning', 'Veuillez valider votre compte pour plus de fonctionnalités.');
        }

        // On recupere les messages de l'utilisateur
        $messages = $repo->findUserMessage($user);

        if ($request->isMethod('POST')) {

            $message = $repo->findOneBy(['id' => $request->request->get("_message")]);
            $message->setAnswer($request->request->get("_content"));

            $em->persist($message);
            $em->flush();

            $this->addFlash('success', 'Votre réponse a bien été envoyée !');
        }


        return $this->render('account/index.html.twig', [
            'user' => $user,
            'messages' => $messages,
        ]);
    }

    /**
     * Get the products of a Seller in "My Account" Section
     * 
     * @IsGranted("ROLE_SELLER")
     * @Route("/vendeur/{id}/mes-articles", name="seller_products")
     *
     * @return void
     */
    public function sellerProducts(int $id)
    {
        $user = $this->getUser();

        if ($id !== $user->getId()) {
            return $this->redirectToRoute('main');
        }

        return $this->render('account/my_products.html.twig', [
            'user' => $user,
            'products' => $user->getProducts(),
        ]);
    }

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/mon-compte/{id}/editer", name="edit_account")
     */
    public function updateInfo(User $user, Request $request, EntityManagerInterface $em)
    {

        // Si l'ID de la route ne corrrespond pas a l'ID de l'user
        if ($this->getUser()->getId() !== $user->getId()) {
            return $this->redirectToRoute('my_account', ['id' => $user->getId()]);
        }

        // Si le compte n'est pas acctiver
        if ($user->getActivationToken() !== null) {
            $this->addFlash('warning', 'Veuillez valider votre compte avant de modifier les infos.');
            return $this->redirectToRoute('my_account', ['id' => $user->getId()]);
        }


        // Création du formulaire en fonction du Type de Compte
        if ($this->isGranted('ROLE_SELLER')) {
            $form = $this->createForm(SellerEditType::class, $user);            
        }
        else {
            $form = $this->createForm(UserEditType::class, $user);
        }
        $form->handleRequest($request);


        // Traitement des données
        if ($form->isSubmitted() && $form->isValid()) {

            $em->persist($user);
            $em->flush();

            $this->addFlash('success', 'Les infos de votre compte ont bien étés mis a jour');
            return $this->redirectToRoute('my_account', ['id' => $user->getId()]);
        }


        return $this->render('account/edit.html.twig', [
            'user' => $user,
            'accountEdit' => $form->createView(),
        ]);
    }

    /**
     * Become a Seller
     * 
     * @IsGranted("ROLE_USER")
     * @Route("/mon-compte/{id}/devenir-vendeur", name="become_seller")
     *
     * @param User $user
     * @param Request $request
     * @return void
     */
    public function becomeSeller(int $id, User $user, Request $request)
    {
        // Si l'utilisateur n'est pas celui attendu, ou qu'il est déja vendeur
        if ($id !== $this->getUser()->getId() || $this->getUser()->getRoles() == "ROLE_SELLER") {
            // on le redirige vers son compte
            return $this->redirectToRoute('my_account', ['id' => $this->getUser()->getId()]);
        }

        // On crée le form vendeur est on prepare la request 'submit'
        $form = $this->createForm(SellerType::class, $user);
        $form->handleRequest($request);


        // Si le form est envoyé et validé, alors on le traite
        if ($form->isSubmitted() && $form->isValid()) {

            // On donne a l'utilisateur un role Vendeur
            $user->setRoles(["ROLE_SELLER"]);

            // On récupère le manager de Doctrine, on prépare et on exécute l'enregistrement de l'user
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            // On informe l'utilisateur, et on le redirige vers la page déposer une annonce
            $this->addFlash('success', 'Vous êtes desormais vendeur ! Veuillez vous connectez a nouveau, et déposer dès maintenant une annonce.');
            return $this->redirectToRoute('my_account', ['id' => $user->getId()]);
        }

        return $this->render('account/becomeseller.html.twig', [
            'formSeller' => $form->createView(),
        ]);
    }
    
}