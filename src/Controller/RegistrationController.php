<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\RegistrationFormType;
use App\Repository\UserRepository;
use App\Security\UserAuthenticator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;

class RegistrationController extends AbstractController
{
    /**
     * @Route("/inscription", name="app_register")
     */
    public function register(Request $request, UserPasswordEncoderInterface $passwordEncoder, \Swift_Mailer $mailer, TokenGeneratorInterface $generateToken): Response 
    {
        $user = new User();

        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // encode the plain password
            $user->setPassword(
                $passwordEncoder->encodePassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );

            // On genere un token 
            $token = $generateToken->generateToken();

            // On attribue a l'user 
            $user->setRoles(["ROLE_USER"]);
            $user->setActivationToken($token);
            $user->setCreatedAt(new \DateTime());
            $user->setRgpd(true);


            // On recupere le manager de Doctrine, et on envoi la requete
            // d'enregistrement de l'utilisateur
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();


            // On crée le message avec swiftmailer, et on l'envoi a l'user
            $message = (new \Swift_Message('Activation du compte'))
                ->setFrom('no-reply@labroquinerie.com')
                ->setTo($user->getEmail())
                ->setBody(
                    $this->renderView(
                        'emails/activation.html.twig',
                        ['token' => $user->getActivationToken(), 'name' => $user->getUsername()]
                    ),
                    'text/html'
                );
            $mailer->send($message);

            // On previent l'utilisateur, et redirection à la page de connexion
            $this->addFlash('warning', 'Veuillez consulter votre boite mail, pour valider votre compte.');
            return $this->redirectToRoute('main');
        }

        return $this->render('registration/register.html.twig', [
            'registrationForm' => $form->createView(),
        ]);
    }

    /**
     * Activate account from mail after inscription
     * 
     * @Route("/activation/{token}", name="app_activation_account")
     *
     * @param string $token
     * @param UserRepository $repo
     * @return void
     */
    public function activation(string $token, UserRepository $repo)
    {
        // Verifier q'un utilisateur existe avec le token donné dans l'URL
        $user = $repo->findOneBy(['activation_token' => $token]);

        // Si il n'y a pas d'utilisateur, on renvoi une erreur 404
        if (!$user) {
            throw $this->createNotFoundException('Cet utilisateur n\'existe pas ..');
        }

        // Sinon on set le token a null
        $user->setActivationToken(null);

        // On prepare et execute la requete 
        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();

        // On previent l'utilisateur, et on redirige vers la page connexion
        $this->addFlash('success', 'Votre compte est desormais activé !');
        return $this->redirectToRoute('app_login');
    }
}
