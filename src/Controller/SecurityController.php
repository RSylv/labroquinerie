<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\NewPasswordType;
use App\Form\ResetPasswordType;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;

class SecurityController extends AbstractController
{
    /**
     * @Route("/connexion", name="app_login")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        if ($this->getUser()) {
            return $this->redirectToRoute('main');
        }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', [
            'last_username' => $lastUsername, 
            'error' => $error
        ]);
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout()
    {
        throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }

    /**
     * Forgotten Password Interface
     * 
     * @Route("/oubli-pass", name="app_forgotten_password")
     *
     * @return void
     */
    public function forgottenPassword(UserRepository $repo, Request $request, \Swift_Mailer $mailer, TokenGeneratorInterface $tokenGenerator) 
    {
        $form = $this->createForm(ResetPasswordType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // On recupere les données du form, et on verifie la presence de l'email dans la bdd
            $data = $form->getData();
            $user = $repo->findOneByEmail($data['email']);

            // Si l'user n'existe pas on renvoi un flashMessage et on sort.
            if (!$user) {
                $this->addFlash('warning', 'Cet email n\'existe pas sur le site');
                return $this->redirectToRoute('app_login');
            }

            // Generation du token
            $token = $tokenGenerator->generateToken();

            
            try {
                $user->setResetToken($token);
                $em = $this->getDoctrine()->getManager();
                $em->persist($user);
                $em->flush();
            } catch (\Exception $e) {
                $this->addFlash('warning', 'Une erreur est survenue : ' . $e->getMessage());
                return $this->redirectToRoute('app_login');
            }

            $url = $this->generateUrl('app_reset_password', (['token' => $token]), UrlGeneratorInterface::ABSOLUTE_URL);

            $message = (new \Swift_Message('Mot de passe oublié'))
                ->setFrom('no-reply@labroquinerie.com')
                ->setTo($user->getEmail())
                ->setBody(
                    $this->renderView('emails/forgotten_pass.html.twig', ['url' => $url]),
                    'text/html'
                )
            ;
            $mailer->send($message);

            $this->addFlash('success', 'Un email vient de vous etre envoyé a l\'adresse saisie.');
            return $this->redirectToRoute('app_login');

        }


        return $this->render('security/forgotten_password.html.twig', [
            'resetForm' => $form->createView(),
        ]);
    }

    /**
     * Reset password
     * 
     * @Route("/oubli-pass/{token}", name="app_reset_password")
     *
     * @return void
     */
    public function resetPassword(string $token, Request $request, UserPasswordEncoderInterface $encoder) 
    {
        // On recherche le token recu dans la BDD, ainsi que l'user lié au token
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository(User::class)->findOneBy(['reset_token' => $token]);

        // Si l'user n'existe pas, on renvoi une erreur, et on redirige l'user 
        if (!$user) {
            $this->addFlash('warning', 'Lien invalide..');
            return $this->redirectToRoute('app_login');
        }

        $form = $this->createForm(NewPasswordType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            // On efface le token , et on  enregistre le Password
            $user->setResetToken(null);
            $user->setPassword(
                $encoder->encodePassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );
            $em->persist($user);
            $em->flush();

            // On informe l'utilisateur, et on le redirige
            $this->addFlash('success', 'Mot de passe modifié avec succès');
            return $this->redirectToRoute('app_login');
        }
        else {
            return $this->render('security/reset_password.html.twig', [
                'formNewPass' => $form->createView(),
                'token' => $token
            ]);
        }
    }
}

