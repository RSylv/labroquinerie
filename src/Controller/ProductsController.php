<?php

namespace App\Controller;

use App\Entity\Image;
use App\Entity\Message;
use App\Entity\Product;
use App\Form\MessageType;
use App\Form\ProductType;
use App\Form\SearchProductType;
use App\Repository\ProductRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;


class ProductsController extends AbstractController
{

    /**
     * Home page for All the Products
     * 
     * @IsGranted("ROLE_USER")
     * @Route("/produits", name="products")
     *
     * @param ProductRepository $repo
     * @return void
     */
    public function index(ProductRepository $repo, Request $request) 
    {

        // Formulaire de recherche Avancée :
        $form = $this->createForm(SearchProductType::class);
        $form->handleRequest($request);

        // Si le form est submit et valid :
        if ($form->isSubmitted() && $form->isValid()) {

            // On donne au prix une valeur par defaut
            $form->get('price')->getData() !== null ? $price = $form->get('price')->getData() : $price = 10000;

            // On appele la fonction de recherche
            $search = $repo->findBySearch(
                $form->get('state')->getData(),
                $form->get('category')->getData()->getId(),
                $price
            );

            // On renvoi dans la vue le nouveau tableau des produits
            return $this->render('products/index.html.twig', [
                'query'      => null,
                'products'   => $search,
                'searchForm' => $form->createView(),
            ]);
        }

        return $this->render('products/index.html.twig', [
            'products'   => $repo->getActiveProducts(),
            'query'      => null,
            'searchForm' => $form->createView(),
        ]);
    }


    /**
     * SearchBar_ Search into products title
     *
     * @Route("/produits/resultats", name="search_products", methods={"POST"})
     * @IsGranted("ROLE_USER", statusCode=404, message="Vous devez etre membre pour aller plus loin sur le site !")
     *
     * @param Request $request
     * @param ProductRepository $repo
     * @return void
     */
    public function handleSearch(Request $request, ProductRepository $repo)
    {
        // Récuperer la requete
        (string) $search = $request->request->get("_query");

        // Appeler la function dans le repository
        $products = $repo->findBySearchBar($search);

        // Rendre la vue en affinant la variable $film
        return $this->render('products/index.html.twig', [
            'products' => $products,
            'query' => $search,
        ]);
    }


    /**
     * Add and Update a Product
     * 
     * @IsGranted("ROLE_SELLER")
     * @Route("/vendeur/produit/{id<[0-9]+>}/editer", name="edit_product")
     * @Route("/vendeur/produit/ajouter", name="add_product")
     *
     * @param Product $product
     * @param Request $request
     * @return void
     */
    public function addProduct(Product $product = null, Request $request)
    {
        // Si le produit n'existe pas on crée un nouvel objet Product
        if (!$product) {
            $product = new Product();
        }

        // On crée le formulaire, et on le prepare la requete
        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);

        // Si le form Envoyé est Validé    
        if ($form->isSubmitted() && $form->isValid()) 
        {
            // On récupere l'user 
            $user = $this->getUser();

            // On vérifie la validitée du token. Si le token n'est pas valide, on renvoi une erreur
            if (!$this->isCsrfTokenValid('create-product' . $user->getId(), $request->request->get('_token'))) 
            {
                $this->addFlash('warning', 'Erreur d\'identification lors de l\'enregistrement.');
            }
            // Si le token est valide
            else 
            {
                // Récupération des donnée(s) Image(s) du form
                $images = $form->get('image')->getData();

                // Si plus de 5 images, oon retourne une erreur, et on sort
                if (count($images) > 5) 
                {
                    $this->addFlash('danger', 'le nombre d\'images est limité a 5');
                }
                // Sinon traitement de données
                else 
                {
                    // TRAITEMENT DES PHOTOS :
                    foreach ($images as $image) {

                        // String Unique pour génerer nom de fichier
                        $imgUrl = md5(uniqid()) . '.' . $image->guessExtension();

                        // Envoi vers le dossier via le paramètre "images_directory" (config/services.yaml)
                        $image->move(
                            $this->getParameter('images_directory'),
                            $imgUrl
                        );

                        // Stock du nom de l'image dans la DataBase
                        $img = new Image();
                        $img->setName($imgUrl);
                        $product->addImage($img);
                    }

                    // On sélectionne le vendeur et la date
                    $product->setSeller($this->getUser());
                    $product->setCreatedAt(new \DateTime());

                    // On recupère le manager, on prepare et on exécute la requete
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($product);
                    $em->flush();

                    // On informe l'utilisateur, et on redirige vers la page "seller_products"
                    $this->addFlash('success', 'Le produit "' . $product->getTitle() . '", a bien été ajoué !');
                    return $this->redirectToRoute('seller_products', ['id' => $user->getId()]);
                }
            } 
        }

        // On renvoi dans la vue, le form, le mode (édition ou création) et le produit
        return $this->render('products/addproduct.html.twig', [
            'formProduct' => $form->createView(),
            'editMode' => $product->getId() !== null,
            'product' => $product
        ]);
    }


    /**
     * Get a Product
     * 
     * @IsGranted("ROLE_USER")
     * @Route("/produit/{id<[0-9]+>}", name="get_product")
     *
     * @param Product $product
     * @return void
     */
    public function getProduct(Product $product, Request $request)
    {
        // On récupère les variables pour le formulaire de message
        $seller = $product->getSeller();
        $user = $this->getUser();
        $message = new Message();

        $form = $this->createForm(MessageType::class, $message);
        $form->handleRequest($request);        

        // Traitement du formulaire du message
        if ($form->isSubmitted() && $form->isValid()) {

            // On rempli l'object Message, avec les infos manquante, puis on l'enregistre.
            $message->setProduct($product)
                ->setFromId($user)
                ->setToId($seller)
                ->setCreatedAt(new \DateTime())
            ;

            $em = $this->getDoctrine()->getManager();
            $em->persist($message);
            $em->flush();

            // On informe l'utilisateur, et on redirige vers la page 'mon-compte'
            $this->addFlash('success', 'Message bien envoyé !');
            return $this->redirectToRoute('my_account', ['id' => $user->getId()]);
        }

        return $this->render('products/show.html.twig', [
            'msgForm' => $form->createView(),
            'product' => $product,
            'seller' => $seller,
        ]);
    }


    /**
     * Delete a Product
     * 
     * @IsGranted("ROLE_SELLER")
     * @Route("/vendeur/produits/supprimer/{id}", name="remove_product", methods="DELETE")
     *
     * @param Request $request
     * @param Product $product
     * @return void
     */
    public function deleteProduct(Request $request, Product $product, AccessDecisionManagerInterface $access)
    {
        // On verifie la validité du token, si OK on continue
        if ($this->isCsrfTokenValid('delete' . $product->getId(), $request->request->get('_token'))) {

            // On recupere le manager de Doctrine, et on supprime
            $em = $this->getDoctrine()->getManager();
            $em->remove($product);
            $em->flush();

            // On informe l'user
            $this->addFlash('success', 'L\'article a bien été supprimé.');

            // On recupere l'user pour determiner son status
            $user = $this->getUser();
            $token = new UsernamePasswordToken($user, 'none', 'none', $user->getRoles());

            // Si c'est un admin on le redige vers le panneau de gestion des produits
            if ($access->decide($token, ["ROLE_ADMIN"])) {
                return $this->redirectToRoute('admin_products');
            }
            else 
            // Sinon On le redirige sur sa page de gestion des produits
            { return $this->redirectToRoute('seller_products', ['id' => $user->getId()]); }
        } 
        // Si le token n'est pas valide on renvoi une erreur 400 => Bad Request - La syntaxe de la requête est erronée. 
        else {
            return new JsonResponse(['error' => 'Le code de suppression est invalide.'], 400);
        }
        
        return $this->redirectToRoute('main');
    }


    /**
     * Delete an Image of the Product
     *
     * @IsGranted("ROLE_SELLER")
     * @Route("/vendeur/produits/supprimer/image/{id<[0-9]+>}", name="remove_image", methods={"DELETE"})
     *
     * @param Image $image
     * @param Request $request
     * @return void
     */
    public function deleteImage(Image $image, Request $request)
    {
        $data = json_decode($request->getContent(), true);

        // On vérifie si le token est valide
        if ($this->isCsrfTokenValid('delete' . $image->getId(), $data['_token'])) {
            // On récupère le nom de l'image
            $nom = $image->getName();
            // On supprime le fichier
            unlink($this->getParameter('images_directory') . '/' . $nom);

            // On supprime l'entrée de la base
            $em = $this->getDoctrine()->getManager();
            $em->remove($image);
            $em->flush();

            // On répond en json
            return new JsonResponse(['success' => 1]);
        } else {
            return new JsonResponse(['error' => 'Token Invalide'], 400);
        }
    }
    
}
