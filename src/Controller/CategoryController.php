<?php

namespace App\Controller;

use App\Entity\Type;
use App\Repository\CategoryRepository;
use App\Repository\TypeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class CategoryController extends AbstractController
{
    /**
     * @Route("/categories", name="categories")
     */
    public function index(CategoryRepository $catRepo, TypeRepository $typeRepo)
    {
        return $this->render('category/index.html.twig', [
            'categories'     => $catRepo->findAll(),
            'types'     => $typeRepo->findAll(),
        ]);
    }
}
