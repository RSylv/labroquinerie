<?php

namespace App\Controller;

use App\Services\Cart\CartService;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class CartController extends AbstractController
{
    /**
     * @IsGranted("ROLE_USER")
     * @Route("/panier", name="cart")
     */
    public function cart(CartService $cartService)
    {
        // On recupere l'user present 
        $user = $this->getUser();

        // Si l'user n'a pas valider son compte
        if ($user->getActivationToken() !== null) {
            $this->addFlash('warning', 'Veuillez valider votre compte pour plus de fonctionnalités.');
        }

        return $this->render('cart/index.html.twig', [
            'cartdata' => $cartService->getFullCart(),
            'total' => $cartService->getTotal()
        ]);
    }

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/panier/add/{id}", name="cart_add")
     */
    public function addInCart(int $id, CartService $cartService)
    {
        $cartService->add($id);

        return $this->redirectToRoute('cart');
    }

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/panier/remove/{id}", name="cart_remove")
     */
    public function removeInCart(int $id, CartService $cartService)
    {  
        $cartService->remove($id);

        $this->addFlash('success', 'panier mis a jour !');

        return $this->redirectToRoute('cart');
    }


}
