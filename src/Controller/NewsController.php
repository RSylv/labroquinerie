<?php

namespace App\Controller;

use App\Entity\News;
use App\Form\NewsType;
use App\Repository\NewsRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Finder\Exception\AccessDeniedException;

class NewsController extends AbstractController
{
    /**
     * @Route("/news", name="news")
     */
    public function index(NewsRepository $repo)
    {
        return $this->render('news/index.html.twig', [
            'news' => $repo->findAll(),
        ]);
    }

    /**
     * @Route("/news/{id<[0-9]+>}", name="show_news")
     */
    public function showNews(News $news)
    {
        return $this->render('news/showNews.html.twig', [
            'news' => $news,
        ]);
    }

    /**
     * Undocumented function
     * 
     * IsGranted("ROLE_ADMIN")
     * @Route("/admin/news/ajouter", name="add_news")
     * @Route("/admin/news/{id<[0-9]+>}/editer", name="edit_news")
     *
     * @param News $news
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return void
     */
    public function addNews(News $news = null, Request $request, EntityManagerInterface $em)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        if (!$news) {
            $news = new News();
        }

        $form = $this->createForm(NewsType::class, $news);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $news->setCreatedAt(new \DateTime());

            $em->persist($news);
            $em->flush();
            
            $this->addFlash('success', 'La nouvelle '.$news->getTitle().' est en ligne !');
            return $this->redirectToRoute('show_news', ['id' => $news->getId()]);
        }
        

        return $this->render('news/addNews.html.twig', [
            'formNews' => $form->createView(),
            'editNews' => $news->getId() !== null, 
            'news'     => $news
        ]);
    }

    /**
     * Delete a news
     * 
     * IsGranted("ROLE_ADMIN")
     * @Route("/admin/news/{id<[0-9]+>}/supprimer", name="remove_news", methods={"DELETE"})
     *
     * @param integer $id
     * @return void
     */
    public function removeNews(News $news) 
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        // try {
            $em = $this->getDoctrine()->getManager();
            $em->remove($news);
            $em->flush();
    
            $this->addFlash('success', 'La news '.$news->getTitle().' a bien été supprimée !');
            return $this->redirectToRoute('news');
        // } 
        // catch (\Throwable $th) {
            // $this->addFlash('warning', 'Un problème est survenu : '.$th->getMessage());
        // } 

    }
}
