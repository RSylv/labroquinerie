<?php

namespace App\Controller;

use App\Repository\ProductRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    /**
     * @Route("/", name="main")
     */
    public function index(ProductRepository $repo)
    {
        $lastProducts= $repo->getLastProducts(3);

        return $this->render('main/index.html.twig', [
            'products' => $lastProducts,
        ]);
    }
}
