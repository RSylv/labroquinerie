<<La Broquinerie
<<Site de petites annonces, vente de CD, DVD et Livres.

---------------------------------------------------------------------------

--> TASKS

---------------------------------------------------------------------------

# ACCEDER AU SITE 
   <X Inscription>
        - (sécuriser les données)
   <X Connexion>

# ANNONCES
   <X Create-Read-Update-Delete>
        - (Voir les Annonces)
        - (Editer les Annonces)
        - (supprimer une Annonce)

# MON COMPTE
   <X Devenir un Vendeur>

   <X Read-Update-Delete>
        - (Visualiser mes Infos)
        - (Modifier mes Infos)

# PANIER
   <X Ajouter une Annonce>
   <X Supprimer une Annonce>
   <X Gestion des quantitées>
   <X Passer commande>

# NEWS
   <X Create-Read-Update-Delete>
        - (Voir les annonces)
        - (Voir une annonce)
        - (Ajouter une annonce)
        - (Modifier une annonce)
        - (Supprimer une annonce)

# Personnaliser 404, 500 ...

# DEVENIR VENDEUR ? 
   <X Ajouter le champ isSellerActive, a la table User>
   <X 'set-role-vendeur' au depot de la première annonce>
   <X créer function dans repository User>

# FORM PRODUCT :: a debugger

# FORM IMBRIQUE




------------------------------------------------------------------
# presentation
# somaire
# charte graphique
# shema navigation site
# bdd
# relation entre données
# presentation